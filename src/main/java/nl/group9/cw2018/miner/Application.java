package nl.group9.cw2018.miner;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.group9.cw2018.miner.domain.Block;
import nl.group9.cw2018.miner.domain.BlockContent;
import nl.group9.cw2018.miner.domain.BlockHeader;
import nl.group9.cw2018.miner.domain.Difficulty;
import nl.group9.cw2018.miner.exception.NewerTeamBlockFound;
import nl.group9.cw2018.miner.hash.Hasher;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;

public class Application {

    private static final String SERVER_ADDRESS = "http://10.0.10.20:9000";
    //private static final String SERVER_ADDRESS = "http://localhost:8080";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final Hasher HASHER = new Hasher();
    private static final String NONCE_PLACEHOLDER = String.format("%20d", new Random().nextInt(1000000000));
    private static final TypeReference<List<Block>> BLOCK_LIST_TYPE = new TypeReference<List<Block>>() {};
    private static int difficulty = 0;
    private static Block leadingTeamBlock = null;

    public static void main(String[] args) throws Exception {
        refreshLeadingTeamBlock();
        while (true) {
            try {
                refreshDifficulty();
                Block previousBlock = leadingTeamBlock != null ? leadingTeamBlock : retrieveLeadingBlock();
                Block minedBlock = mineNewBlock(previousBlock);
                postBlock(minedBlock);
                refreshLeadingTeamBlock();
            } catch (NewerTeamBlockFound e) {
                System.out.println("Newer team block found.");
            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
            }

        }
    }

    private static Block mineNewBlock(Block previousBlock) throws IOException, NewerTeamBlockFound {
        Block block = new Block();
        block.content = new BlockContent();
        block.header = new BlockHeader();
        block.header.contentHash = HASHER.hash(block.content.toString(), 0);
        block.header.previousHash = previousBlock.hash;
        block.header.blockNumber = previousBlock.header.blockNumber + 1;

        Integer nonce = 0;
        boolean found = false;
        String hash = null;
        block.header.nonce = NONCE_PLACEHOLDER;
        String tempBlockHeaderString = block.header.toString();
        byte[] headerBytes = tempBlockHeaderString.getBytes(StandardCharsets.UTF_8);
        int nonceOffset = tempBlockHeaderString.indexOf(NONCE_PLACEHOLDER);

        long startTime = System.currentTimeMillis();
        while (!found) {
            nonce++;
            String nonceString = nonce.toString();
            block.header.nonce = nonceString + NONCE_PLACEHOLDER.substring(nonceString.length(), 20);
            byte[] nonceBytes = block.header.nonce.getBytes(StandardCharsets.UTF_8);
            for (int i = 0; i < 20; i++) {
                headerBytes[nonceOffset + i] = nonceBytes[0 + i];
            }
            hash = HASHER.hash(headerBytes, difficulty);
            found = hash != null;
            if (!found && (nonce & 0x7ffff) == 0) {
                refreshDifficulty();
                refreshLeadingTeamBlock();
                if (leadingTeamBlock != null && !leadingTeamBlock.hash.equals(previousBlock.hash)) {
                    throw new NewerTeamBlockFound();
                }
            }
        }
        System.out.println(String.format("Hash found after %d ms. Nonce: %d", System.currentTimeMillis() - startTime, nonce));

        block.hash = hash;
        return block;
    }

    private static Block retrieveLeadingBlock() throws IOException {
        Response response = Request.Get(SERVER_ADDRESS + "/blocks/end-of-chain").execute();
        String blockJson = response.returnContent().asString();
        return OBJECT_MAPPER.readValue(blockJson, Block.class);
    }

    private static void refreshLeadingTeamBlock() throws IOException {
        Response response = Request.Get(SERVER_ADDRESS + "/blocks").execute();
        String blocksJson = response.returnContent().asString();
        List<Block> blocks = OBJECT_MAPPER.readValue(blocksJson, BLOCK_LIST_TYPE);
        Block leadingBlock = null;
        for (Block block: blocks) {
            if (block.isTeamBlock()) {
                if (leadingBlock == null) {
                    leadingBlock = block;
                } else {
                    if (block.header.blockNumber > leadingBlock.header.blockNumber ||
                            (block.header.blockNumber == leadingBlock.header.blockNumber) && (block.hash.compareTo(leadingBlock.hash) > 0)) {
                        leadingBlock = block;
                    }
                }
            }
        }
        leadingTeamBlock = leadingBlock;
    }

    private static void refreshDifficulty() throws IOException {
        difficulty = retrieveDifficulty();
        System.out.println("Difficulty: " + difficulty);
    }

    private static int retrieveDifficulty() throws IOException {
        Response response = Request.Get(SERVER_ADDRESS + "/difficulty").execute();
        String difficultyJson = response.returnContent().asString();
        return OBJECT_MAPPER.readValue(difficultyJson, Difficulty.class).value;
    }

    private static boolean postBlock(Block block) throws IOException {
        Response response = Request.Post(SERVER_ADDRESS + "/blocks")
                .bodyString(block.toString(), ContentType.APPLICATION_JSON)
                .execute();
        HttpResponse returnResponse = response.returnResponse();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        returnResponse.getEntity().writeTo(byteArrayOutputStream);
        System.out.println(byteArrayOutputStream.toString());
        boolean success = returnResponse.getStatusLine().getStatusCode() == 201;
        if (success) {
            System.out.println(block);
        }
        return success;
    }
}