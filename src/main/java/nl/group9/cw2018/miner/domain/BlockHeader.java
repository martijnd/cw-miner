package nl.group9.cw2018.miner.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlockHeader {

    public int blockNumber;
    public String previousHash;
    public String contentHash;
    public String nonce;

    @Override
    public String toString() {
        return "{\"blockNumber\":" + blockNumber + ",\"previousHash\":\"" + previousHash + "\",\"contentHash\":\"" + contentHash + "\",\"nonce\":\"" + nonce + "\"}";
    }
}
