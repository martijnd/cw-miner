package nl.group9.cw2018.miner.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PointAssignment {

    public String target;

    public int points;
}
