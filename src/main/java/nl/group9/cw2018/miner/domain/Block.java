package nl.group9.cw2018.miner.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Block {
    public String hash;
    public BlockHeader header;
    public BlockContent content;

    @Override
    public String toString() {
        return "{\"hash\":\"" + hash + "\",\"header\":" + header + ", \"content\":"  + content + "}";
    }

    public boolean isTeamBlock() {
        List<PointAssignment> pointDistribution = this.content.pointDistribution;
        return pointDistribution.size() == 1 && pointDistribution.get(0).target.equals("TAP");
    }
}
