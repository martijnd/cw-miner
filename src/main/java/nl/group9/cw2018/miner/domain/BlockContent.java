package nl.group9.cw2018.miner.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlockContent {

    public List<PointAssignment> pointDistribution;

    @Override
    public String toString() {
        return "{\"pointDistribution\":[{\"target\":\"TAP\",\"points\":100}]}";
    }
}
