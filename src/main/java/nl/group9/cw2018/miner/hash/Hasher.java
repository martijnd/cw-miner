package nl.group9.cw2018.miner.hash;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hasher {

    private MessageDigest digest;

    public Hasher() {
        try {
            this.digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public String hash(String content, int difficulty) {
        byte[] contentBytes = content.getBytes(StandardCharsets.UTF_8);
        return hash(contentBytes, difficulty);
    }

    public String hash(byte[] content, int difficulty) {
        byte[] hash = this.digest.digest(content);
        if (leadingZeroCount(hash) >= difficulty) {
            return toHex(hash);
        } else {
            return null;
        }
    }

    private static int leadingZeroCount(byte[] hash) {
        int totalCountLeadingZeros = 0;
        byte[] var4 = hash;
        int var5 = hash.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            byte b = var4[var6];
            int lzc = leadingZeroCountOfByte(b);
            totalCountLeadingZeros += lzc;
            if (lzc != 8) {
                break;
            }
        }

        return totalCountLeadingZeros;
    }

    private static int leadingZeroCountOfByte(byte b) {
        int i = b & 255;
        return Integer.numberOfLeadingZeros(i) - 24;
    }

    private String toHex(byte[] bytes) {
        StringBuffer hexString = new StringBuffer();

        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xff & bytes[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }

        return hexString.toString();
    }
}
