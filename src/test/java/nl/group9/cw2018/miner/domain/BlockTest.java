package nl.group9.cw2018.miner.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BlockTest {

    @Test
    public void toStringTest() {
        Block block = new Block();
        block.hash = "1234";
        block.content = new BlockContent();
        block.header = new BlockHeader();
        block.header.blockNumber = 1;
        block.header.contentHash = "1111";
        block.header.previousHash = "4321";
        block.header.nonce = "bla";

        String blockString = block.toString();

        assertEquals(blockString, "{\"hash\":\"1234\",\"header\":{\"blockNumber\":1,\"previousHash\":\"4321\",\"contentHash\":\"1111\",\"nonce\":\"bla\"}, \"content\":{\"pointDistribution\":[{\"target\":\"Martijn\",\"points\":100}]}}");
    }
}
